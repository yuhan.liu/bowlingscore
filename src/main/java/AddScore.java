class AddScore {
    private int ball;
    private int[] totalThrows = new int[21];
    private int itsCurrentThrow = 0;

    void addThrow(int point) {
        totalThrows[itsCurrentThrow++] = point;
    }

    int scoreForGround() {
        ball = 0;
        int score = 0;
        for (int currentFrame = 0; currentFrame < 10; currentFrame++) {
            if (strike()) {
                score += 10 + nextTwoBallsForStrike();
                ball++;
            } else if (spare()) {
                score += 10 + nextBallForSpare();
                ball += 2;
            } else {
                score += twoBallsInFrame();
                ball += 2;
            }
        }
        return score;
    }

    private boolean strike() {
        return totalThrows[ball] == 10;
    }

    private int nextTwoBallsForStrike() {
        return totalThrows[ball + 1] + totalThrows[ball + 2];
    }

    private boolean spare() {
        return (totalThrows[ball] + totalThrows[ball + 1]) == 10;
    }

    private int nextBallForSpare() {
        return totalThrows[ball + 2];
    }

    private int twoBallsInFrame() {
        return totalThrows[ball] + totalThrows[ball + 1];
    }
}
