public class BowlingScore {
    private int totalScore = 0;
    private AddScore itsAddScore = new AddScore();
    private int theCurrentRound = 0;
    private boolean theFirstThrowInLastGround = true;

    public void givePoint(int[] input) {
        BowlingScore bowlingScore = new BowlingScore();
        bowlingScore.addScoreAndRound(input);
        this.totalScore = bowlingScore.scoreForGround();
    }

    public int getTotalScore() {
        return totalScore;
    }

    private void addScoreAndRound(int[] pins) {
        for (int num : pins) {
            itsAddScore.addThrow(num);
            adjustCurrentGround(num);
        }
    }

    private int scoreForGround() {
        return itsAddScore.scoreForGround();
    }

    private boolean isItStrike(int point) {
        return (theFirstThrowInLastGround && point == 10);
    }

    private void adjustCurrentGround(int point) {
        if (lastBallInGround(point))
            advanceGround();
        else
            theFirstThrowInLastGround = false;
    }

    private boolean lastBallInGround(int point) {
        return isItStrike(point) || !theFirstThrowInLastGround;
    }

    private void advanceGround() {
        theCurrentRound = Math.min(10, theCurrentRound + 1);
    }
}