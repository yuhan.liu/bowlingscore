import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BowlingTest {
    @Test
    public void should_get_total_point_when_no_point() {
        BowlingScore bowlingScore = new BowlingScore();
        assertEquals(0, bowlingScore.getTotalScore());
    }

    @Test
    public void should_get_total_point_when_one_throw() {
        BowlingScore bowlingScore = new BowlingScore();
        bowlingScore.givePoint(new int[]{8});
        assertEquals(8, bowlingScore.getTotalScore());
    }

    @Test
    public void should_get_total_point_when_two_throw() {
        BowlingScore bowlingScore = new BowlingScore();
        bowlingScore.givePoint(new int[]{8, 1});
        assertEquals(9, bowlingScore.getTotalScore());
    }

    @Test
    public void should_get_total_point_when_10_frame_without_strike_and_spare() {
        BowlingScore bowlingScore = new BowlingScore();
        bowlingScore.givePoint(new int[]{4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4});
        assertEquals(80, bowlingScore.getTotalScore());
    }

    @Test
    public void should_get_total_point_when_first_frame_is_spare() {
        BowlingScore bowlingScore = new BowlingScore();
        bowlingScore.givePoint(new int[]{5, 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4});
        assertEquals(86, bowlingScore.getTotalScore());
    }

    @Test
    public void should_get_total_point_with_spare_and_last_without_squre() {
        BowlingScore bowlingScore = new BowlingScore();
        bowlingScore.givePoint(new int[]{5, 5, 1, 6, 1, 6, 1, 6, 5, 5, 1, 6, 1, 6, 1, 6, 1, 6, 1, 6});
        assertEquals(78, bowlingScore.getTotalScore());
    }

    @Test
    public void should_get_total_point_with_spare() {
        BowlingScore bowlingScore = new BowlingScore();
        bowlingScore.givePoint(new int[]{2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 5, 5, 1});
        assertEquals(47, bowlingScore.getTotalScore());
    }

    @Test
    public void should_get_total_point_when_first_throw_is_strike() {
        BowlingScore bowlingScore = new BowlingScore();
        bowlingScore.givePoint(new int[]{10, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2});
        assertEquals(50, bowlingScore.getTotalScore());
    }

    @Test
    public void should_get_total_point_and_last_without_strike() {
        BowlingScore bowlingScore = new BowlingScore();
        bowlingScore.givePoint(new int[]{10, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 10, 2, 2});
        assertEquals(60, bowlingScore.getTotalScore());
    }

    @Test
    public void should_get_total_point_with_strike() {
        BowlingScore bowlingScore = new BowlingScore();
        bowlingScore.givePoint(new int[]{2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 10, 2, 1});
        assertEquals(49, bowlingScore.getTotalScore());
    }

    @Test
    public void should_get_300_with_all_strike() {
        BowlingScore bowlingScore = new BowlingScore();
        bowlingScore.givePoint(new int[]{10,10,10,10,10,10,10,10,10,10,10,10});
        assertEquals(300, bowlingScore.getTotalScore());
    }
}